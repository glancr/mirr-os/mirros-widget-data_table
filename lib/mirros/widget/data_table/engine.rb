module Mirros
  module Widget
    module DataTable
      class Engine < ::Rails::Engine
        isolate_namespace Mirros::Widget::DataTable
        config.generators.api_only = true
      end
    end
  end
end
