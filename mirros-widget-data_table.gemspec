require 'json'
$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "mirros/widget/data_table/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "mirros-widget-data_table"
  spec.version     = Mirros::Widget::DataTable::VERSION
  spec.authors     = ["Tobias Grasse"]
  spec.email       = ["tg@glancr.de"]
  spec.homepage    = ""
  spec.summary     = "Displays a two-column table on your glancr."
  spec.description = "Edit and display a two-column table."
  spec.license     = "MIT"
  spec.metadata    = { 'json' =>
    {
      type: 'widgets',
      title: {
        enGb: 'Table',
        deDe: 'Tabelle',
        frFr: 'Tableau',
        esEs: '',
        plPl: '',
        koKr: ''
      },
      description: {
        enGb: spec.description,
        deDe: '',
        frFr: '',
        esEs: '',
        plPl: '',
        koKr: ''
      },
      group: nil, # TODO
      compatibility: '0.0.0',
      sizes: [
        { w: 4, h: 4 }
        # TODO: Default size is 4x4, add additional sizes if your widget supports them.
      ],
      # Add all languages for which your Vue templates are fully translated.
      languages: [:enGb],
      single_source: false # Change to true if your widget doesn't aggregate data from multiple sources.
    }.to_json
  }

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 5.2.6"
end
