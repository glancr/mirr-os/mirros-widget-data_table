module Mirros
  module Widget
    module DataTable
      class Configuration < WidgetInstanceConfiguration
        attribute :rows, :string, default: '<tr><td>Lorem ipsum</td><td>dolor sit amet</td></tr><tr><td>Lorem ipsum</td><td>dolor sit amet</td></tr>'
        attribute :header_left_col, :string, default: 'Left'
        attribute :header_right_col, :string, default: 'Right'
        attribute :show_column_headers, :boolean, default: true
        attribute :dotted_line, :boolean, default: false

        validates :show_column_headers, boolean: true
      end
    end
  end
end
